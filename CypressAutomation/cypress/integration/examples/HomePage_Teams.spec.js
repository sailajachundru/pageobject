
describe('Home Page validation', () => {
    it('Test Case 1', () => {
        cy.visit("http://localhost:3000/");
        cy.get("input[type='email']").type("shailaja.chundru@pearson.com");
        cy.get("input[type='password']").type("JOINED-snow-GUARD-MATTER");
        cy.contains('Login').click();
        cy.wait(3000);
        cy.get("span:contains('Home')").should('contain', 'Home');
        cy.contains("Your Workspace").should('contain', 'Your Workspace');
        //cy.get("div:contains('SC')").click();
        cy.wait(3000);
        cy.get('.styles__InitialsAvatar-sc-1bv089f-1').click();
        cy.get("div:contains('Theme')").should('contain', 'Theme');
        cy.wait(3000);
        cy.get("div:contains('Logout')").should('contain', 'Logout');
        
    })
})
