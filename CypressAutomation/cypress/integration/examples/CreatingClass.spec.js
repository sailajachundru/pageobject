
describe('Creating a Class', () => {
    it('Testcase 1', () => {
        cy.visit("https://prod-aero.bronte.prd-prsn.com/");
        cy.get("input[type='email']").type("shailaja.chundru@pearson.com");
        cy.get("input[type='password']").type("JOINED-snow-GUARD-MATTER");
        cy.contains("Login").click();
        cy.wait(3000);
        cy.get("#menu").click();
        cy.wait(3000);
        cy.get("a div div").eq(3).click();
        cy.wait(3000);
        cy.get("#add").click();
      //  cy.get(".className-input-3").type("Test Class 20");
        cy.get('#className-input-3').type("Test Class 20");
        cy.contains("Create class").click();
    })
})