

//import { wrap } from 'cypress/types/lodash';

require('cypress-xpath')

describe('Iframe testcase', async () => {
  it('Testcase 1', () => {
    cy.visit("http://localhost:3000/");
    cy.get("input[type='email']").type("shailaja.chundru@pearson.com");
    cy.get("input[type='password']").type("JOINED-snow-GUARD-MATTER");
    cy.contains('Login').click();
    cy.wait(3000);
    cy.get("#menu").click();
    cy.wait(3000);
    cy.get("a div div").eq(1).click();
    cy.get("span:contains('Test Lesson 15')").click();
    cy.wait(20000);
    cy.get("div[class='PreviewStack__Content-sc-158h5q9-3 dTOXfg']", { timeout: 10000 }).click();
    cy.wait(3000);
    console.log("Before Frame")
    cy.get("iframe[src='https://plugin-distribution-bronte-dev.pearson.com/5769bf90-575b-11ea-8ed8-ff18d76b3e83/c8182f3c8e71ac76111fdd086f4cb926/dist/index.html']").then(function ($nestedFrame) {
      var ifNestedFrame = $nestedFrame.contents().find("iframe[title='Test Iframe']").contents().find("input[aria-labelledby='emailAddress-label-1']");
      cy.wrap(ifNestedFrame).type("shailaja.chundru@pearson.com")
      console.log("Enter iframe")
    })
    })
})
