/**
 * @author Shailaja Chundru
 * @description Cypress Dropdown
 */

describe('Dynamic Dropdown testcase', function () {
    it('Testcase 1', function () {
        cy.visit("https://www.seleniumeasy.com/test/drag-and-drop-demo.html");

        cy.get('#todrag>span:nth-child(2)').drag('#mydropzone');
    })
})
