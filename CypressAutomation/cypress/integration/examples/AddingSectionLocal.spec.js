//import '.commands';
describe('Add section to screen', function ()  {
    it('Adding Screen to the Lesson', function () {
        cy.visit('http://localhost:3000/');
        cy.get("input[type='email']").type("shailaja.chundru@pearson.com");
        cy.get("input[type='password']").type("JOINED-snow-GUARD-MATTER");
        cy.contains('Login').click();
        cy.wait(3000);
        cy.get("#menu").click();
        cy.wait(3000);
        cy.get('[href="/courseware"] > .PageNav__PageNavItem-sc-1hljpsq-3').click();
        cy.wait(1000);
        cy.get("i[id='add']").click();
        cy.get("button div").eq(2).click();
        cy.wait(1000);
        cy.get("button:contains('Blank')").click();
        cy.get("button div").eq(1).click();
        cy.get("select").select("FREE").should('contain', 'Free');
        cy.contains('Continue').click();
        cy.wait(3000);
        cy.get('.PreviewStack__Content-sc-158h5q9-3').click();
        cy.wait(3000);
        cy.get("#close").click();
        cy.wait(3000);
        cy.get("#add").click();
        cy.wait(6000);
        cy.get("button:contains('Blank')").click();
        cy.wait(3000);
        cy.contains('A screen of a lesson').click();
        cy.wait(3000);

        // iframe handling with jquery
        // cy.xpath("//div[@class='qnyfl0-1 eCRwLm stage-box']").then(function ($ele) {
        //     var ifele = $ele.contents().find("div[text='Add Section']");
        //     //wrap function
        //     cy.wrap(ifele).click();
    //})
        // iframe handling
        cy.iframe("iframe[src='https://plugin-distribution-bronte-dev.pearson.com/5769bf90-575b-11ea-8ed8-ff18d76b3e83/1c965701147458fdfdfadf306ab8723c/dist/index.html']").find("button[class='sc - 1i0le39 - 0 jleBSC']").should('be.visible').should('contain', 'Add Section').click();
})
})
