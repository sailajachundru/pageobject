describe('alert handling', function () {
    beforeEach(function () {
        cy.visit("http://the-internet.herokuapp.com/javascript_alerts");
    })
    it.only('testcase1', function () {
        cy.contains('Click for JS Alert').click();
    })

    it('Testcase2', function () {
        cy.wait(3000);
        cy.on('window:confirm', function (confirmText) {
            return false
        })
        cy.contains('Click for JS Confirm').click()
    })

    it('Testcase3', function () {
        cy.window().then(function ($win) {
            cy.stub($win, 'prompt').returns('Hiiii');
            cy.contains('Click for JS Prompt').click()
        })
    })
})
