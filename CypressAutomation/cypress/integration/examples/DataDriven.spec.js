describe('Data Driven', function()  {

    before(function () {
        cy.fixture('example').then(function (data) {
            this.data = data;
        })
    })
    it('test case1', function() {
        cy.visit('http://www.facebook.com');
        cy.get('#email').eq(0).type(this.data.username)
        cy.get('#pass').eq(0).type(this.data.password)
    })
})
