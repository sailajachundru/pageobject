
describe('Invisible element', () => {
    it('Handleinvisible element', () => {
        cy.visit("https://rahulshettyacademy.com/seleniumPractise/#/");
        cy.get("input[type='search']").type('ca')
        cy.wait(2000)
        cy.get(".product:visible").should('have.length', 4)
        cy.get("h4[class='product-name']").eq(0).should('have.text', 'Cauliflower - 1 Kg')
        cy.get(".product-name:visible").eq(0).should(($tx) =>
        {
            const text = $tx.text();
            expect(text).to.match(/Cauliflower/)
            expect(text).to.include('1 Kg')
        })
    })
})
