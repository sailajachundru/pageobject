import 'cypress-iframe'

describe('iframe testsuite', async () => {
    it('test case1', () => {
        //  cy.visit("https://rahulshettyacademy.com/AutomationPractice/");

        //  cy.visit("https://www.amazon.com");
        //  cy.xpath("//a[text()='Customer Service']").click()

        cy.visit("http://the-internet.herokuapp.com/nested_frames");

        cy.xpath("//frame[@name='frame-top']").then(function ($iframeParentelement) {
            var ifeleNestedFrame = $iframeParentelement.contents().find("frame[name='frame-left']").contents().find("html body")
            //wrap function
            cy.wrap(ifeleNestedFrame).click();

            cy.log(ifeleNestedFrame.text())
        })
    })
})