describe('Dynamic Dropdown testcase', function () {
    it('Testcase 1', function () {
        cy.visit("http://the-internet.herokuapp.com/checkboxes");

        cy.xpath("(//input[@type='checkbox'])[1]").check().should('be.checked').and('not.have.value');
        cy.xpath("(//input[@type='checkbox'])[2]").uncheck().should('not.be.checked')
    })
})
